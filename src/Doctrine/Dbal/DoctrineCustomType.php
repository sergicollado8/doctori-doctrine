<?php

declare(strict_types=1);

namespace DoctorI\Shared\Doctrine\Infrastructure\Doctrine\Dbal;

interface DoctrineCustomType
{
    public static function customTypeName(): string;
}
